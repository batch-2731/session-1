// Activity part 1 Quiz

// 1. How do you create arrays in JS?
// -enclosing the data in a array literal ("[ ])

// 2. How do you access the first character of an array?
// -arr[0]

// 3. How do you access the last character of an array?
// -arr[arr.length-1]

// 4. What array method searches for, and returns the index of a given value in an array?
// a. This method returns -1 if given value is not found in the array.
// -indexOf()

// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
// -forEach()

// 6. What array method creates a new array with elements obtained from a user-defined function?
// -map()

// 7. What array method checks if all its elements satisfy a given condition?
// -every()

// 8. What array method checks if atleast one of its elements satisfies a given condition?
// -some()

// 9. True or False: array.splice() modifies a copy of the array, leaving the original unchaged.
// -true

// 10. True or False. array.slice() copies elements from original array and return these as a new array.
// -true

//  Part 2

/* 1. Create a function named addToEnd that will add a passed in string to
the end of a passed in array. If element to be added is not a string,
return the string "error - can only add strings to an array". Otherwise,
return the updated array. Use the students array and the string "Ryan"
as arguments when testing. */

function addToEnd(array, name) {
  if (typeof name !== "string") {
    return "error - can only add strings to an array";
  } else {
    array.push(name);
    return array;
  }
}

/*2. Create a function named addToStart that will add a passed in string to
the start of a passed in array. If element to be added is not a string, return
the string "error - can only add strings to an array". Otherwise, return the
updated array. Use the students array and the string "Tess" as arguments
when testing. */

function addToStart(array, name) {
  if (typeof name !== "string") {
    return "error - can only add strings to an array";
  } else {
    array.unshift(name);
    return array;
  }
}

/*3. Create a function named elementChecker that will check a passed in
array if at least one of its elements has the same value as a passed in
argument. If array is empty, return the message "error - passed in array is
empty". Otherwise, return a boolean value depending on the result of the
check. Use the students array and the string "Jane" as arguments when
testing. */

function elementChecker(array, name) {
  if (array.length === 0) {
    return "error - passed in array is empty";
  } else {
    return array.includes(name);
  }
}

/* 4. Create a function named checkAllStringsEnding that will accept a passed in array and
a character. The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must
be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
data type string"
● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
a single character"
● if every element in the array ends in the passed in character, return true. Otherwise
return false.
Use the students array and the character "e" as arguments when testing. */

function checkAllStringsEnding(arr, char) {
  if (arr.length === 0) {
    return "error - array must NOT be empty";
  }
  if (!arr.every((s) => typeof s === "string")) {
    return "error - all array elements must be strings";
  }
  if (typeof char !== "string") {
    return "error - 2nd argument must be of data type string";
  }
  if (char.length !== 1) {
    return "error - 2nd argument must be a single character";
  }

  return arr.every((s) => s.endsWith(char));
}

const studentsno4 = ["Jane", "Kate", "Sue", "Alice", "Anne"];

console.log(checkAllStringsEnding(studentsno4, "e"));

/* 5. Create a function named stringLengthSorter that will take in an array of
strings as its argument and sort its elements in an ascending order based
on their lengths. If at least one element is not a string, return "error - all
array elements must be strings". Otherwise, return the sorted array. Use
the students array to test. */

function stringLengthSorter(array) {
  let isArrayStringOnly = array.every((element) => {
    return typeof element === "string";
  });

  // console.log(isArrayStringOnly);

  if (isArrayStringOnly) {
    const ascending = array.sort((a, b) => a.length - b.length);

    return ascending;
  } else {
    return "error - all array elements must be strings";
  }
}

/* 6. Create a function named startsWithCounter that will take in an array of strings and a
single character. The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must
be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
data type string"
● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
a single character"
● return the number of elements in the array that start with the character argument,
must be case-insensitive
Use the students array and the character "J" as arguments when testing. */

function startsWithCounter(arr, char) {
  if (arr.length === 0) {
    return "error - array must NOT be empty";
  }
  if (!arr.every((s) => typeof s === "string")) {
    return "error - all array elements must be strings";
  }
  if (typeof char !== "string") {
    return "error - 2nd argument must be of data type string";
  }
  if (char.length !== 1) {
    return "error - 2nd argument must be a single character";
  }

  let count = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].toLowerCase().startsWith(char.toLowerCase())) {
      count++;
    }
  }

  return count;
}

const students = ["John", "Mary", "Jessica", "James", "Jordan"];

console.log(startsWithCounter(students, "J"));

/* 7. Create a function named likeFinder that will take in an array of strings and a string to
be searched for. The function will do the ff:
● if array is empty, return "error - array must NOT be empty"
● if at least one array element is NOT a string, return "error - all array elements must
be strings"
● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
data type string"
● return a new array containing all elements of the array argument that have the string
argument in it, must be case-insensitive
Use the students array and the character "jo" as arguments when testing. */

function likeFinder(arr, str) {
  if (arr.length === 0) {
    return "error - array must NOT be empty";
  }
  if (!arr.every((s) => typeof s === "string")) {
    return "error - all array elements must be strings";
  }
  if (typeof str !== "string") {
    return "error - 2nd argument must be of data type string";
  }

  const searchStr = str.toLowerCase();

  return arr.filter((s) => s.toLowerCase().includes(searchStr));
}

const studentsno7 = ["Jane", "Kate", "Sue", "Alice", "Anne", "John", "Joe"];

console.log(likeFinder(studentsno7, "jo"));
